# README #

Copyright 2017 Andrew W. Zuelsdorf. All rights reserved.

### What is this repository for? ###

This repository holds the source code and installer for JWFM Sentry,
which is software that uses network connections to identify
malware and other unwanted programs even in the event that conventional
antivirus erroneously mark these programs as being "safe" or "trusted"
or have a scan interval too low to catch these programs while they are in
use. This allows early threat detection and mitigation regardless of virus
signature.

### How do I get set up? ###

Run the installer or open the solution in Visual Studio.

### Contribution guidelines ###

Clone the repository and submit a pull request.

### Who do I talk to? ###

Email andrew.z1@yandex.com with any questions or comments you have.